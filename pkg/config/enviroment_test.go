package config

import (
	"github.com/joho/godotenv"
	"log"
	"reflect"
	"testing"
)

func Test_getEnv(t *testing.T) {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
	type args struct {
		key        string
		defaultVal string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Test №1: Successful",
			args: args{
				"ROUTER_ADDR",
				"localhost:8080",
			},
			want: "localhost:8060",
		},
		{
			name: "Test №2: Unsuccessful",
			args: args{
				"ROUTER",
				"localhost:8080",
			},
			want: "localhost:8080",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getEnv(tt.args.key, tt.args.defaultVal); got != tt.want {
				t.Errorf("getEnv() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getEnvAsSlice(t *testing.T) {
	if err := godotenv.Load("../.env"); err != nil {
		log.Print("No .env file found")
	}
	type args struct {
		name       string
		defaultVal []string
		sep        string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Test №1: Successful",
			args: args{
				"TIMEFRAMES",
				[]string{"5m", "1h"},
				",",
			},
			want: []string{"1m", "5m", "15m", "30m", "1h"},
		},
		{
			name: "Test №2: Unsuccessful",
			args: args{
				"TIMEFRAME_LOCAL",
				[]string{"5m", "1h"},
				",",
			},
			want: []string{"5m", "1h"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getEnvAsSlice(tt.args.name, tt.args.defaultVal, tt.args.sep); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getEnvAsSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}
