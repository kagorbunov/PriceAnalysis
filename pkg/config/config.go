package config

import "time"

func New() *Config {
	t, _ := time.ParseDuration(getEnv("TIMEOUT", "5m"))
	return &Config{
		Router: &Router{
			AddrService:       getEnv("ROUTER_ADDR", "localhost:8080"),
			AddrPriceHistory:  getEnv("ROUTER_ADDR_PRICE_HISTORY", "localhost:8070"),
			AddrCurrencyPairs: getEnv("ROUTER_ADDR_CURRENCY_PAIRS", "localhost:8090"),
		},
		Timeframes: getEnvAsSlice("TIMEFRAMES", []string{"15m", "1h"}, ","),
		TIMEOUT:    t,
	}
}

type Config struct {
	Router     *Router
	Timeframes []string
	TIMEOUT    time.Duration
}

type Router struct {
	AddrService       string
	AddrPriceHistory  string
	AddrCurrencyPairs string
}
