package contract

type CurrencyPairs struct {
	Currency []string `json:"currency"`
}

type Transactions struct {
	Transaction []*Transaction
}

type Transaction struct {
	Name  string         `json:"name"`
	Value []*PriceInTime `json:"value"`
}

type PriceInTime struct {
	Data  string `json:"data"`
	Price string `json:"price"`
}

type RequestPriceHistory struct {
	Name      string `json:"name"`
	StartData string `json:"start_data"`
	StopData  string `json:"stop_data"`
}
