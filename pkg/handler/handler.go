package handler

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/kagorbunov/PriceAnalysis/services/OHLC"
	"net/http"
)

type Handler struct {
	ctx    context.Context
	Router *mux.Router
	ohlc   *OHLC.AllOHLC
}

func NewHandler(ctx context.Context, Router *mux.Router, ohlc *OHLC.AllOHLC) *Handler {
	return &Handler{ctx: ctx, Router: Router, ohlc: ohlc}
}

func (h *Handler) InitializeRoutes() {
	h.Router.HandleFunc("/ohlc", h.GetAllOHLC).Methods("GET")
	h.Router.HandleFunc("/ohlc/name/{name}", h.GetOHLCName).Methods("GET")
	h.Router.HandleFunc("/ohlc/timeframe/{timeframe}", h.GetOHLCTimeframe).Methods("GET")
	h.Router.HandleFunc("/ohlc/{name}/{timeframe}", h.GetOHLCNameTimeframe).Methods("GET")
}

func (h *Handler) GetAllOHLC(w http.ResponseWriter, r *http.Request) {
	respondWithJSON(w, http.StatusOK, h.ohlc)
}

func (h *Handler) GetOHLCName(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["name"]

	var response []*OHLC.OHLC
	for _, el := range h.ohlc.AllOHLC {
		if el.Name == name {
			response = append(response, el)
		}
	}
	respondWithJSON(w, http.StatusOK, &OHLC.AllOHLC{AllOHLC: response})
}

func (h *Handler) GetOHLCTimeframe(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	timeframe := vars["timeframe"]

	var response []*OHLC.OHLC
	for _, el := range h.ohlc.AllOHLC {
		if el.Timeframe == timeframe {
			response = append(response, el)
		}
	}
	respondWithJSON(w, http.StatusOK, &OHLC.AllOHLC{AllOHLC: response})
}

func (h *Handler) GetOHLCNameTimeframe(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["name"]
	timeframe := vars["timeframe"]

	var response []*OHLC.OHLC
	for _, el := range h.ohlc.AllOHLC {
		if el.Name == name && el.Timeframe == timeframe {
			response = append(response, el)
		}
	}
	respondWithJSON(w, http.StatusOK, &OHLC.AllOHLC{AllOHLC: response})
}
