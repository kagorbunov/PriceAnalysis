package handler

import (
	"bytes"
	"encoding/json"
	"gitlab.com/kagorbunov/PriceAnalysis/pkg/contract"
	"gitlab.com/kagorbunov/PriceAnalysis/services/OHLC"
	"log"
	"net/http"
	"sync"
	"time"
)

func GetCurrencyPairs(url string) *contract.CurrencyPairs {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	defer resp.Body.Close()

	var result *contract.CurrencyPairs
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	return result
}

func GetTransaction(name, urlTran string) *contract.Transaction {
	t := time.Now().Format(time.Stamp)
	StartData, err := OHLC.GetTimeStartToDay()
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	reqPH := &contract.RequestPriceHistory{
		Name:      name,
		StartData: StartData.Format(time.Stamp),
		StopData:  t,
	}
	response, _ := json.Marshal(reqPH)
	resp, err := http.Post(urlTran, "application/json", bytes.NewBuffer(response))
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	defer resp.Body.Close()

	var result *contract.Transaction
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Fatalln(err)
		return nil
	}
	return result
}

func GetTransactions(urlCP, urlTran string, mutex *sync.Mutex) *contract.Transactions {
	mutex.Lock()
	var listTransactions []*contract.Transaction
	cPairs := GetCurrencyPairs(urlCP)
	for _, cPair := range cPairs.Currency {
		t := GetTransaction(cPair, urlTran)
		listTransactions = append(listTransactions, t)
	}
	mutex.Unlock()
	return &contract.Transactions{Transaction: listTransactions}
}
