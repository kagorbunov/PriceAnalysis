# Golang final exam
## Transaction Price Analysis Service
## Task №3
Postman description :
https://documenter.getpostman.com/view/19090990/VUjSGixJ

Postman description JSON :
https://www.postman.com/collections/81203f5c65946e716205
## Run locally

Please run the services in order.
1) Start task №1
2) Start task №2
3) Start task №3

- Build and run:

```bash
$ git clone https://gitlab.com/kagorbunov/PriceAnalysis.git
$ cd PriceAnalysis
$ go mod download
$ sudo make run 
```

- Run tests:
```bash 
$ sudo make test
```

- Run lint:
```bash 
$ sudo make lint-run
```

Server is listening on http://localhost:8060

[POST] http://localhost:8070/transaction_history

{
"name":"USDJPY",
"start_data" : "Aug 14 00:00:00",
"stop_data"  : "Aug 14 18:15:35"
}

All configuration parameters in .env