package OHLC

import (
	"fmt"
	"gitlab.com/kagorbunov/PriceAnalysis/pkg/contract"
	"log"
	"math"
	"strconv"
	"sync"
	"time"
)

type AllOHLC struct {
	AllOHLC []*OHLC `json:"allOHLC"`
}

type OHLC struct {
	Name       string        `json:"name"`
	Timeframe  string        `json:"timeframe"`
	OHLCMarker []*OHLCMarker `json:"ohlcMarker"`
}

type OHLCMarker struct {
	Number int    `json:"number"`
	Open   string `json:"open"`
	High   string `json:"high"`
	Low    string `json:"low"`
	Close  string `json:"close"`
}

func Calculate(tr *contract.Transactions, timeframes []string, mutex *sync.Mutex) *AllOHLC {
	mutex.Lock()
	var arrayOHLC []*OHLC
	if len(timeframes) == 0 {
		return nil
	}
	for _, el := range tr.Transaction {
		for _, t := range timeframes {
			if t == "" {
				continue
			}
			ohlc := calculateOne(el, t)
			arrayOHLC = append(arrayOHLC, ohlc)
		}
	}
	mutex.Unlock()
	return &AllOHLC{
		AllOHLC: arrayOHLC,
	}
}

func calculateOne(tr *contract.Transaction, timeframe string) *OHLC {
	timeframeDuration, err := time.ParseDuration(timeframe)
	if err != nil {
		log.Fatalf("Error parsing timeframe: %v", err)
		return nil
	}
	t, _ := GetTimeStartToDay()
	numbers := math.Ceil(time.Now().Add(3*time.Hour).Sub(t).Seconds() / timeframeDuration.Seconds())
	var arrayOHLC []*OHLCMarker

	for i := 1; i <= int(numbers); i++ {
		arrayOfPrice := getArrayOfPrice(tr, timeframe, int64(i))
		ohlc := calculateOHLC(arrayOfPrice)
		if ohlc == nil {
			continue
		}
		arrayOHLC = append(
			arrayOHLC,
			&OHLCMarker{
				Number: i,
				Open:   ohlc[0],
				High:   ohlc[1],
				Low:    ohlc[2],
				Close:  ohlc[3],
			},
		)
	}
	return &OHLC{
		Name:       tr.Name,
		Timeframe:  timeframe,
		OHLCMarker: arrayOHLC,
	}
}

func getArrayOfPrice(tr *contract.Transaction, timeframe string, number int64) (arrayOfPrice []float64) {
	timeStartData, err := GetTimeStartToDay()
	if err != nil {
		log.Fatalf("Error getting data: %v", err)
		return nil
	}
	timeframeDuration, err := time.ParseDuration(timeframe)
	if err != nil {
		log.Fatalf("Error parsing timeframe: %v", err)
		return nil
	}
	if number != 1 {
		timeStartData = timeStartData.Add(timeframeDuration * time.Duration(number-1))
	}
	timeStopData := timeStartData.Add(timeframeDuration)
	for _, value := range tr.Value {
		data, err := time.Parse(time.Stamp, value.Data)
		data = data.AddDate(time.Now().Year(), 0, 0)
		if err != nil {
			log.Fatalf("Error parsing data: %v", err)
			return nil
		}
		if data.After(timeStartData) && data.Before(timeStopData) {
			price, _ := strconv.ParseFloat(value.Price, 64)
			arrayOfPrice = append(arrayOfPrice, price)
		}
	}
	return
}

func GetTimeStartToDay() (time.Time, error) {
	t := time.Now().UTC().Format(time.Stamp)
	lenT := len(t)
	StartData := t[:lenT-8] + "00:00:00"
	data, err := time.Parse(time.Stamp, StartData)
	return data.AddDate(time.Now().Year(), 0, 0), err
}

func calculateOHLC(values []float64) []string {
	lenArray := len(values)
	arrayOHLCFloat := []float64{0, 0, 0, 0}
	if lenArray == 0 {
		return nil
	}
	arrayOHLC := []string{
		fmt.Sprintf("%v", values[0]),
		fmt.Sprintf("%v", values[0]),
		fmt.Sprintf("%v", values[0]),
		fmt.Sprintf("%v", values[0])}
	if lenArray > 1 {
		arrayOHLCFloat[3] = values[lenArray-1]
		arrayOHLCFloat[1] = values[1]
		arrayOHLCFloat[2] = values[1]
		for _, i := range values {
			if i >= arrayOHLCFloat[1] {
				arrayOHLCFloat[1] = i
			}
			if i <= arrayOHLCFloat[2] {
				arrayOHLCFloat[2] = i
			}
		}
		arrayOHLC[1] = fmt.Sprintf("%v", arrayOHLCFloat[1])
		arrayOHLC[2] = fmt.Sprintf("%v", arrayOHLCFloat[2])
		arrayOHLC[3] = fmt.Sprintf("%v", arrayOHLCFloat[3])
	}
	return arrayOHLC
}
