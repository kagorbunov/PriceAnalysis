package OHLC

import (
	"gitlab.com/kagorbunov/PriceAnalysis/pkg/contract"
	"reflect"
	"sync"
	"testing"
	"time"
)

func TestCalculate(t *testing.T) {
	type args struct {
		tr         *contract.Transactions
		timeframes []string
		mutex      *sync.Mutex
	}
	var tests = []struct {
		name string
		args args
		want *AllOHLC
	}{{
		"Test №1: Successful",
		args{
			tr: &contract.Transactions{
				Transaction: []*contract.Transaction{
					{
						Name: "USDRUB",
						Value: []*contract.PriceInTime{
							{
								Data:  "Aug 15 00:04:01",
								Price: "70",
							}, {
								Data:  "Aug 15 00:04:02",
								Price: "65",
							},
							{
								Data:  "Aug 15 00:04:03",
								Price: "75",
							}, {
								Data:  "Aug 15 00:04:04",
								Price: "72",
							},
							{
								Data:  "Aug 15 00:07:01",
								Price: "71",
							}, {
								Data:  "Aug 15 00:07:02",
								Price: "74",
							},
							{
								Data:  "Aug 15 00:08:03",
								Price: "69",
							}, {
								Data:  "Aug 15 00:08:04",
								Price: "73",
							},
						},
					}, {
						Name: "USDJPY",
						Value: []*contract.PriceInTime{
							{
								Data:  "Aug 15 00:04:01",
								Price: "131",
							}, {
								Data:  "Aug 15 00:04:02",
								Price: "127",
							},
							{
								Data:  "Aug 15 00:04:03",
								Price: "135",
							}, {
								Data:  "Aug 15 00:04:04",
								Price: "128",
							},
							{
								Data:  "Aug 15 00:07:01",
								Price: "132",
							}, {
								Data:  "Aug 15 00:07:02",
								Price: "136",
							},
							{
								Data:  "Aug 15 00:07:08",
								Price: "133",
							},
							{
								Data:  "Aug 15 00:08:03",
								Price: "128",
							}, {
								Data:  "Aug 15 00:08:04",
								Price: "135",
							},
						},
					},
				},
			},
			timeframes: []string{"5m"},
			mutex:      &sync.Mutex{},
		},
		&AllOHLC{
			AllOHLC: []*OHLC{{
				"USDRUB",
				"5m",
				[]*OHLCMarker{{
					1,
					"70",
					"75",
					"65",
					"72",
				}, {
					2,
					"71",
					"74",
					"69",
					"73",
				}},
			}, {
				"USDJPY",
				"5m",
				[]*OHLCMarker{{
					1,
					"131",
					"135",
					"127",
					"128",
				}, {
					2,
					"132",
					"136",
					"128",
					"135",
				}},
			},
			},
		},
	},
		{
			"Test №2: Unsuccessful",
			args{
				tr: &contract.Transactions{
					Transaction: []*contract.Transaction{
						{
							Name: "USDRUB",
							Value: []*contract.PriceInTime{
								{
									Data:  "Aug 15 00:04:01",
									Price: "70",
								}, {
									Data:  "Aug 15 00:04:02",
									Price: "65",
								},
								{
									Data:  "Aug 15 00:04:03",
									Price: "75",
								}, {
									Data:  "Aug 15 00:04:04",
									Price: "72",
								},
								{
									Data:  "Aug 15 00:07:01",
									Price: "71",
								}, {
									Data:  "Aug 15 00:07:02",
									Price: "74",
								},
								{
									Data:  "Aug 15 00:08:03",
									Price: "69",
								}, {
									Data:  "Aug 15 00:08:04",
									Price: "73",
								},
							},
						}, {
							Name: "USDJPY",
							Value: []*contract.PriceInTime{
								{
									Data:  "Aug 15 00:04:01",
									Price: "131",
								}, {
									Data:  "Aug 15 00:04:02",
									Price: "127",
								},
								{
									Data:  "Aug 15 00:04:03",
									Price: "135",
								}, {
									Data:  "Aug 15 00:04:04",
									Price: "128",
								},
								{
									Data:  "Aug 15 00:07:01",
									Price: "132",
								}, {
									Data:  "Aug 15 00:07:02",
									Price: "136",
								},
								{
									Data:  "Aug 15 00:07:08",
									Price: "133",
								},
								{
									Data:  "Aug 15 00:08:03",
									Price: "128",
								}, {
									Data:  "Aug 15 00:08:04",
									Price: "135",
								},
							},
						},
					},
				},
				timeframes: []string{""},
				mutex:      &sync.Mutex{},
			},
			&AllOHLC{
				AllOHLC: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Calculate(tt.args.tr, tt.args.timeframes, tt.args.mutex); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Calculate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetTimeStartToDay(t *testing.T) {
	tests := []struct {
		name    string
		want    time.Time
		wantErr bool
	}{
		{
			name:    "Test №1: Successful",
			want:    time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetTimeStartToDay()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTimeStartToDay() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTimeStartToDay() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculateOHLC(t *testing.T) {
	type args struct {
		values []float64
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Test №1: Successful",
			args: args{
				values: []float64{125.25, 162.05, 132, 54},
			},
			want: []string{"125.25", "162.05", "54", "54"},
		},
		{
			name: "Test №2: Successful",
			args: args{
				values: []float64{125.25},
			},
			want: []string{"125.25", "125.25", "125.25", "125.25"},
		},
		{
			name: "Test №3: Unsuccessful",
			args: args{
				values: []float64{},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateOHLC(tt.args.values); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("calculateOHLC() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculateOne(t *testing.T) {
	type args struct {
		tr        *contract.Transaction
		timeframe string
	}
	tests := []struct {
		name string
		args args
		want *OHLC
	}{
		{
			name: "Test №1: Successful",
			args: args{
				tr: &contract.Transaction{
					Name: "USDRUB",
					Value: []*contract.PriceInTime{
						{
							Data:  "Aug 15 00:04:01",
							Price: "70",
						}, {
							Data:  "Aug 15 00:04:02",
							Price: "65",
						},
						{
							Data:  "Aug 15 00:04:03",
							Price: "75",
						}, {
							Data:  "Aug 15 00:04:04",
							Price: "72",
						},
						{
							Data:  "Aug 15 00:07:01",
							Price: "71",
						}, {
							Data:  "Aug 15 00:07:02",
							Price: "74",
						},
						{
							Data:  "Aug 15 00:08:03",
							Price: "69",
						}, {
							Data:  "Aug 15 00:08:04",
							Price: "73",
						},
					},
				},
				timeframe: "5m",
			},
			want: &OHLC{
				"USDRUB",
				"5m",
				[]*OHLCMarker{{
					1,
					"70",
					"75",
					"65",
					"72",
				}, {
					2,
					"71",
					"74",
					"69",
					"73",
				}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateOne(tt.args.tr, tt.args.timeframe); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("calculateOne() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getArrayOfPrice(t *testing.T) {
	type args struct {
		tr        *contract.Transaction
		timeframe string
		number    int64
	}
	tests := []struct {
		name             string
		args             args
		wantArrayOfPrice []float64
	}{
		{
			name: "Test 1: Successful",
			args: args{
				tr: &contract.Transaction{
					Name: "USDRUB",
					Value: []*contract.PriceInTime{
						{
							Data:  "Aug 15 00:04:01",
							Price: "70",
						}, {
							Data:  "Aug 15 00:04:02",
							Price: "65",
						},
						{
							Data:  "Aug 15 00:04:03",
							Price: "75",
						}, {
							Data:  "Aug 15 00:04:04",
							Price: "72",
						},
						{
							Data:  "Aug 15 00:07:01",
							Price: "71",
						}, {
							Data:  "Aug 15 00:07:02",
							Price: "74",
						},
						{
							Data:  "Aug 15 00:08:03",
							Price: "69",
						}, {
							Data:  "Aug 15 00:08:04",
							Price: "73",
						},
					},
				},
				timeframe: "5m",
				number:    2,
			},
			wantArrayOfPrice: []float64{71, 74, 69, 73},
		},
		{
			name: "Test 2: Unsuccessful",
			args: args{
				tr: &contract.Transaction{
					Name: "USDRUB",
					Value: []*contract.PriceInTime{
						{
							Data:  "Aug 15 00:04:01",
							Price: "70",
						}, {
							Data:  "Aug 15 00:04:02",
							Price: "65",
						},
						{
							Data:  "Aug 15 00:04:03",
							Price: "75",
						}, {
							Data:  "Aug 15 00:04:04",
							Price: "72",
						},
						{
							Data:  "Aug 15 00:07:01",
							Price: "71",
						}, {
							Data:  "Aug 15 00:07:02",
							Price: "74",
						},
						{
							Data:  "Aug 15 00:08:03",
							Price: "69",
						}, {
							Data:  "Aug 15 00:08:04",
							Price: "73",
						},
					},
				},
				timeframe: "5m",
				number:    3,
			},
			wantArrayOfPrice: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotArrayOfPrice := getArrayOfPrice(tt.args.tr, tt.args.timeframe, tt.args.number); !reflect.DeepEqual(gotArrayOfPrice, tt.wantArrayOfPrice) {
				t.Errorf("getArrayOfPrice() = %v, want %v", gotArrayOfPrice, tt.wantArrayOfPrice)
			}
		})
	}
}
