package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/kagorbunov/PriceAnalysis/pkg/config"
	"gitlab.com/kagorbunov/PriceAnalysis/pkg/handler"
	"gitlab.com/kagorbunov/PriceAnalysis/services/OHLC"
	"log"
	"net/http"
	"sync"
	"time"
)

const appName = "service.PriceAnalysis"

func main() {
	fmt.Println("Project Name: ", appName)

	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cfg := config.New()
	router := mux.NewRouter()
	var mutex sync.Mutex
	tr := handler.GetTransactions(cfg.Router.AddrCurrencyPairs, cfg.Router.AddrPriceHistory, &mutex)
	allOHLC := OHLC.Calculate(tr, cfg.Timeframes, &mutex)
	go func() {
		for {
			tr = handler.GetTransactions(cfg.Router.AddrCurrencyPairs, cfg.Router.AddrPriceHistory, &mutex)
			allOHLC = OHLC.Calculate(tr, cfg.Timeframes, &mutex)
			time.Sleep(cfg.TIMEOUT)
		}
	}()
	h := handler.NewHandler(ctx, router, allOHLC)
	h.InitializeRoutes()
	log.Printf("Start listen and serve : %v", cfg.Router.AddrService)
	log.Fatal(http.ListenAndServe(cfg.Router.AddrService, router))
}
